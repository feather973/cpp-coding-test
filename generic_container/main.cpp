#include <array>
#include <iostream>
#include <type_traits>

/* trailing return type : https://www.ibm.com/docs/de/zos/2.3.0?topic=declarators-trailing-return-type-c11 */
/* variadic template : https://en.cppreference.com/w/cpp/language/parameter_pack */
/* std::forward : https://cplusplus.com/reference/utility/forward/ */
/* typename vs class (almost interchangable) : https://stackoverflow.com/questions/2023977/difference-of-keywords-typename-and-class-in-templates */
/* type alias : https://en.cppreference.com/w/cpp/language/type_alias */
/* rvalue reference(&&, search 'move constructor') : https://stackoverflow.com/questions/5481539/what-does-t-double-ampersand-mean-in-c11 */
/* move constructor : https://docs.microsoft.com/ko-kr/cpp/cpp/move-constructors-and-move-assignment-operators-cpp?view=msvc-170 */
/* rvalue : https://press.rebus.community/programmingfundamentals/chapter/lvalue-and-rvalue/ */
/* auto variable : https://subscription.packtpub.com/book/programming/9781786465184/1/ch01lvl1sec5/using-auto-whenever-possible */
/* common_type<>::type : https://docs.microsoft.com/ko-kr/cpp/standard-library/common-type-class?view=msvc-170 */
/* copy-list-initialization(no need to copy constructor) : https://en.cppreference.com/w/cpp/language/list_initialization#copy-list-initialization */
/* https://stackoverflow.com/questions/21825933/any-difference-between-copy-list-initialization-and-traditional-copy-initializat */



// typename 파라미터팩을 받고 이름을 Args 로 지정
// std::array 는 길이가 필요하다. 파라미터팩에서 sizeof 를 쿼리해서 넘긴다.  
// std::common_type 은 convert 할 type 명이 필요하다. non-type 파라미터팩으로 넘긴다. (no pack name)
// build_array 는 Args 타입을 알아야 리턴형 정의가 가능하므로, -> 뒤에 리턴형 std::array 탬플릿 클래스를 정의한다.

template<typename ... Args>
auto build_array(Args&&... args) -> std::array<typename std::common_type<Args...>::type, sizeof...(args)>
{

// 가독성을 위해 commonType 이라는 type alias 를 생성하였다.

	using commonType = typename std::common_type<Args...>::type;

// args 는 rvalue(임시변수) 이므로 참조하려면 타입값을 알아야한다. forward 를 통해 타입값을 deduce 한다.
// deduce 한 타입값 'commonType' 으로 copy-list-initialization 한다. 별도의 copy initializer 가 없기 때문이다.

	return {std::forward<commonType>((Args&&)args)...};
}

int main()
{
// 리턴값을 =(move) 하면 이동생성자에 의해 객체가 생성된다.
// 생성된 객체는 auto 변수가 맞는 type 을 deduce 해서 받는다.

	auto data = build_array(1, 0u, 'a', 3.2f, false);			/* all data type will cast to float */

	//auto data2 = build_array(1, "Packt", 2.0);				/* compile error */
	for (auto i: data)
		std::cout << i << " ";
	std::cout << std::endl;

	return 0;
}
