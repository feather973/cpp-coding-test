#include <iostream>
#include <deque>

/* https://stackoverflow.com/questions/49289738/how-to-print-a-deque-from-the-end */

/* 
 * std::deque.erase example 
 * https://en.cppreference.com/w/cpp/container/deque/erase
 */

int main()
{
	std::deque<int> deq = {1, 2, 3, 4, 5};

	deq.push_front(0);
	deq.push_back(6);

	deq.insert(deq.begin() + 2, 10);	/* [2] = 10 */
	deq.pop_back();
	deq.pop_front();

	deq.erase(deq.begin() + 1);				/* erase [1] */
	deq.erase(deq.begin() + 3, deq.end());	/* erase [3] ~ [n-1] */
	
	for (auto i = deq.begin(); i != deq.end(); ++i )
		std::cout << *i << std::endl;
}
