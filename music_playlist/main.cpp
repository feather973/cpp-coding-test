#include <iostream>
#include <algorithm>
#include <list>

// reference? ( It's just alias )
// 1) When return, do not return local variable refernece
// 2) When pass vector param, pass reference ( https://en.cppreference.com/book/intro/reference )

// get pointer while list iteration : &*it;
// https://stackoverflow.com/questions/2754650/getting-value-of-stdlistiterator-to-pointer

// Inner class not access to outer class private member, use constructor to pass member value instead.

// iterator basics ( By the way, table is good! )
// https://cplusplus.com/reference/iterator/

// when there is reference member, implicit assignment cannot work
// https://stackoverflow.com/questions/5966698/error-use-of-deleted-function

struct cq_node {
	std::string data;
};

/* add element to list begin, pop element from list end */
/* update front and rear for cq_iter */
class cq
{
public:
	using node = cq_node;
	using node_ptr = node*;
private:
	std::list<node> __list;
	node_ptr front;
	node_ptr rear;
public:
	void enqueue(std::string const &data)		/* not changable (string literal "") */
	{
		auto n = new node { .data = data };
		__list.push_front(*n);

		auto it = __list.begin();
		front = &*it;					/* update */
	}

	std::string dequeue()
	{
		if (__list.empty())
			return "";

		// get return value
		auto it = __list.end();		
		it--;
		auto n = *it;				/* node_ptr */
		auto ret = n.data;			/* node_ptr->data */
		__list.pop_back();
		
		// update rear
		it = __list.end();
		it--;
		rear = &*it;					/* update */

		return ret;
	}

	/* for music_player */
	bool is_empty()
	{
		return __list.empty();
	}

	struct cq_iter
	{
		public:
			using node = cq_node;
			using node_ptr = node*;
		private:
			node_ptr ptr;
			const std::list<node> &____list;
		public:
			/* when pass list, pass with reference to prevent copy whole list */
			cq_iter(node_ptr p, const std::list<node> &l) : ptr(p), ____list(l) { }
			/* when cq initialized, cq_iter need to initialize */
			cq_iter() = default;

			std::string& operator*() { return ptr->data; }
			node_ptr get() { return ptr; }

			/* 전위증가 ( 기존 object reference 반환 ) */
			cq_iter& operator++()
			{
				std::list<node>::const_reverse_iterator it;
				std::list<node>::const_reverse_iterator it_rend;

				for (it = ____list.rbegin(); it != ____list.rend(); ++it)
				{
					if (&*it == ptr) {
						//std::cout << "found : " << &*it << ", " << ptr << std::endl;
						break;
					}
				}

				it_rend = ____list.rend();
				it_rend--;	

				if (&*it == &*it_rend) {
					it = ____list.rbegin();
				} else {
					std::advance(it, 1);			/* foo --> bar ( when reverse_iterator, + is reverse ) */
				}

				ptr = (node_ptr) &*it;				/* change ptr */

				return *this;		/* cq_iter (reference) */
			}

			/* 후위증가 ( temporary object 반환 ) */
			cq_iter operator++(int)			/* int : 후위증가라는 뜻 */
			{
				std::list<node>::const_reverse_iterator it;
				std::list<node>::const_reverse_iterator it_rend;
				cq_iter tmp = *this;
								
				/* reverse order */
				for (it = ____list.rbegin(); it != ____list.rend(); ++it)
				{
					if (&*it == ptr) {
						//std::cout << "found : " << &*it << ", " << ptr << std::endl;
						break;
					}
				}

				it_rend = ____list.rend();
				it_rend--;

				if (&*it == &*it_rend) {
					it = ____list.rbegin();
				} else {
					std::advance(it, 1);			/* foo --> bar ( when reverse_iterator, + is reverse ) */
				}

				ptr = (node_ptr) &*it;				/* change ptr */

				return tmp;			/* cq_iter (value) */
			}

			cq_iter& operator--()
			{
				std::list<node>::const_iterator it;
				std::list<node>::const_iterator it_end;

				for (it = ____list.begin(); it != ____list.end(); ++it)
				{
					if (&*it == ptr) {
						//std::cout << "found : " << &*it << ", " << ptr << std::endl;
						break;
					}
				}

				it_end = ____list.end();
				it_end--;

				if (&*it == &*it_end) {
					it = ____list.begin();
				} else {
					std::advance(it, 1);			/* bar --> foo */
				}

				ptr = (node_ptr) &*it;				/* change ptr */

				return *this;		/* cq_iter (reference) */
			}

			cq_iter operator--(int)
			{
				std::list<node>::const_iterator it;
				std::list<node>::const_iterator it_end;
				cq_iter tmp = *this;

				for (it = ____list.begin(); it != ____list.end(); ++it)
				{
					if (&*it == ptr) {
						//std::cout << "found : " << &*it << ", " << ptr << std::endl;
						break;
					}
				}

				it_end = ____list.end();
				it_end--;

				if (&*it == &*it_end) {
					it = ____list.begin();
				} else {
					std::advance(it, 1);			/* bar --> foo */
				}

				ptr = (node_ptr) &*it;				/* change ptr */

				return tmp;			/* cq_iter (value) */
			}

			/* friend : cq can use this */
			friend bool operator==(const cq_iter &left, const cq_iter &right)
			{
				return left.ptr == right.ptr;
			}

			friend bool operator!=(const cq_iter &left, const cq_iter &right)
			{
				return left.ptr != right.ptr;
			}
	};
	
	cq_iter begin() { return cq_iter(rear, __list); }
	cq_iter begin() const { return cq_iter(rear, __list); }
	cq_iter end() { return cq_iter(front, __list); }
	cq_iter end() const { return cq_iter(front, __list); }
	
	/* key function : current node_ptr --> iterator at current position */
	cq_iter current(node_ptr p) { return cq_iter(p, __list); }

	cq (const std::initializer_list<std::string>& ilist)
	{
		for (auto it = std::begin(ilist); it != std::end(ilist); it++) {
			auto n = new node { .data = *it };		
			__list.insert(__list.begin(), *n);			/* always insert at the begin() */
		}

		std::list<node>::iterator it;
		it = __list.begin();
		front = &*it;					/* front is begin */

		it = __list.end();
		it--;
		rear = &*it;					/* rear is end */
	}
};

class music_player : public cq {
public:
	using node = cq_node;
	using node_ptr = node*;
private:
	node_ptr cur;
public:
	music_player (const std::initializer_list<std::string>& ilist) : cq(ilist) {
		auto it = this->begin();

		cur = it.get();
	}

	/* key feature for music_player */
	std::string const next()
	{
		auto it = this->current(cur);

		//it++;
		++it;
		cur = it.get();

		return cur->data;
	}

	std::string const previous()
	{
		auto it = this->current(cur);

		//it--;
		--it;
		cur = it.get();

		return cur->data;
	}

	std::string const now()
	{
		return cur->data;
	}

	void print_all()
	{
		if (is_empty())
			return;

		auto it = this->current(cur);
		std::cout << *it << std::endl;

		auto start = it;
		it++;

		for (; &*it != &*start; it++) {
			std::cout << *it << std::endl;
		}
	}
};

/* music player */
int main() {
	music_player mp = { "foo", "bar", "baz" };

/* 1) iteration test */
// foo bar baz
	std::cout << "[test1] iteration" << std::endl;
	std::cout << mp.now() << std::endl;
	std::cout << mp.next() << std::endl;
	std::cout << mp.next() << std::endl;

// foo baz
	std::cout << mp.next() << std::endl;
	std::cout << mp.previous() << std::endl;

/* 2) enqueue / dequeue test */
	std::cout << "[test2] enqueue / dequeue" << std::endl;

	std::cout << "enqueue 3 item" << std::endl;
	mp.enqueue("quz1");
	mp.enqueue("quz2");
	mp.enqueue("quz3");
	mp.print_all();
	
	std::cout << "dequeue 2 item" << std::endl;
	mp.dequeue();
	mp.dequeue();
	mp.print_all();

	std::cout << "dequeue 5 item" << std::endl;
	mp.dequeue();
	mp.dequeue();
	mp.dequeue();
	mp.dequeue();
	mp.dequeue();
	mp.print_all();
}
