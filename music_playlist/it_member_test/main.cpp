#include <iostream>
#include <algorithm>
#include <list>

// reference member? ( can initialize once )
// - use initializer list
// - https://stackoverflow.com/questions/15403815/how-to-initialize-the-reference-member-variable-of-a-class

// lvalue, rvalue?
// lvalue represent object that has location of memory. rvalue doesn't.
// - [OK] int i = 10; ( "lvalue" = rvalue )
// - [NOT OK] 10 = i; ( rvalue = lvalue )
// - https://www.tutorialspoint.com/lvalue-and-rvalue-in-c

// function-try-block
// - use it to initialize iterator member
// - https://en.cppreference.com/w/cpp/language/function-try-block

class cq
{
public:
	struct cq_iter;		/* forward declaration */
private:
	int a;
	cq_iter &b;			/* forward declaration need reference member (not object member) */
public:
	struct cq_iter
	{
		private:
			int c;
		public:
#if 0
			cq_iter(int x) { 
				c = x;
			}
#else
			cq_iter(int x) : c(x) { }
#endif

			void print()
			{
				std::cout << "c : " << c << std::endl;
				return;
			}
	};

	cq(int x) try : a{x}, b{*(new cq_iter(x))}
	{}	
	catch(const std::exception& e)
	{
		std::cerr << "cq_iter memory allocation fail\n" << std::endl;
		throw e;
	}

	void print()
	{
		std::cout << "a : " << a << std::endl;
		b.print();
		return;
	}
};

int main() {
	cq *ptr = new cq(10);
	ptr->print();	
}
