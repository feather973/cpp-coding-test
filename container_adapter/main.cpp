#include <iostream>
#include <deque>
#include <stack>
#include <vector>
#include <list>
#include <queue>


int main()
{
	// stack1 : stack should forbid push_front(1)
	std::deque<int> stk1;
	stk1.push_back(1);
	stk1.push_back(2);
	stk1.pop_back();
	stk1.push_front(0);		// {0, 1}
	
	for (auto it1 = stk1.begin(); it1 != stk1.end(); ++it1)
		std::cout << *it1 << std::endl;

	// stack2
	std::stack<int> stk2;
	stk2.push(1);
	stk2.push(2);
	stk2.pop();
	//stk2.push_front(0);		// forbid

	// container adapter has no adapter
# if 0
	for (auto it2 = stk2.begin(); it2 != stk2.end(); ++it2)
		std::cout << *it2 << std::endl;
#endif

	// stack{3,4}
	std::stack<int, std::vector<int>> stk3;
	std::stack<int, std::list<int>> stk4;

	// if FIFO needs, use queue
	std::queue<int> q;

	q.push(1);
	q.push(2);
	q.push(3);
	q.pop();
	q.push(4);
}
