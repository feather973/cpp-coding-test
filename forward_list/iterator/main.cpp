#include <iostream>
#include <forward_list>
#include <vector>

int main()
{
		std::vector<std::string> vec = {
			"Lewis Hamilton", "Lewis Hamilton", "Nico Roseberg", "Sebastian Vettel", "Lewis Hamilton", "Sebastian Vettel", "Sebastian Vettel", "Sebastian Vettel", "Fernando Alonso" 
		};

		auto it = vec.begin();		// O(1) 
		std::cout << "recent winner : " << *it << std::endl;
		
		it += 8;					// O(1)
		std::cout << "winner of 8 years ago : " << *it << std::endl;

		advance(it, -3);			// O(1)
		std::cout << "and later 3 years : " << *it << std::endl;
	
		std::forward_list<std::string> fwd(vec.begin(), vec.end());

		auto it1 = fwd.begin();
		std::cout << "recent winner : " << *it1 << std::endl;

		advance(it1, 5);
		std::cout << "winner of 5 years ago : " << *it1 << std::endl;

		// std::forward only move forward
		// advance(it1, -2);

		// error: no match for ‘operator+=’
		// it1 += 2;
}
