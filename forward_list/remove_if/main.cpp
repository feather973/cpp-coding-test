#include <string>
#include <iostream>
#include <forward_list>

struct citizen
{
	std::string name;
	int age;
};

std::ostream &operator<<(std::ostream &os, const citizen &c)
{
	return (os << "[" << c.name << ", " << c.age << "]");
}

int main()
{
	std::forward_list<citizen> citizens = {
		{"Kim", 22}, {"Lee", 25}, {"Park", 18}, {"Jin", 16}
	};

	auto citizen_copy = citizens;		/* deep copy */

	std::cout << "total : ";
	for (const auto &c : citizens)
		std::cout << c << " ";
	std::cout << std::endl;

	citizens.remove_if([](const citizen &c) {
		return (c.age < 19);
	});

	std::cout << "votable : ";
	for (const auto &c : citizens)
		std::cout << c << " ";
	std::cout << std::endl;

	/* restore previous value */
	citizens = citizen_copy;
	citizens.remove_if([](const citizen &c) {
		return (c.age != 18);	
	});

	std::cout << "votable next year : ";
	for (const auto &c : citizens)
		std::cout << c << " " ;
	std::cout << std::endl;
}
