#include <iostream>
#include <vector>

int main()
{
	std::vector<int> vec = {1,2,3,4,5};

	// no insert : ok
	auto v_it4 = vec.begin() + 4;			// 5
	std::cout << "print [4] : " << *v_it4 << std::endl;

	v_it4--;
	std::cout << "print [3] : " << *v_it4 << std::endl;		// ok

	// insert : fail
	v_it4 = vec.begin() + 4;				// 5
	std::cout << "print [4] : " << *v_it4 << std::endl;

	vec.insert(vec.begin() + 2 , 0);	// v_it4 not valid
	v_it4--;
	std::cout << "print [3] : " << *v_it4 << std::endl;		// oops!
}
