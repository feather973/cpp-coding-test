#include <iostream>
#include <algorithm>

/* 1. node */
struct singly_ll_node
{
	int data;
	singly_ll_node* next;
};

struct singly_ll_iterator;

/* 2. linked list class of node */
class singly_ll
{
public:
		using node = singly_ll_node;
		using node_ptr = node*;

private:
		node_ptr head;

public:
		// before :        head head->next
		// after :     new head head->next
		void push_front(int val)
		{
			auto new_node = new node {val, NULL};
			if (head != NULL)
				new_node->next = head;
			head = new_node;
		}

		void pop_front()
		{
			auto first = head;		// node to delete
			if (head)
			{
				head = head->next;
				delete first;
			}
		}

		/* 3. basic iterator */
		/* add tab to bloced area (after v, enter >>) : https://stackoverflow.com/a/235841 */
		struct singly_ll_iterator
		{
		public:
			using node = singly_ll_node;
			using node_ptr = node*;

		private:
			node_ptr ptr;

		public:
			singly_ll_iterator(node_ptr p) : ptr(p) {}

			int& operator*() {	return ptr->data;	}

			node_ptr get() { return ptr; }

			singly_ll_iterator& operator++()		// ++a : a=a+1, return a
			{
				ptr = ptr->next;
				return *this;
			}

			singly_ll_iterator operator++(int)			// a++ : return a, a=a+1
			{
				singly_ll_iterator result = *this;
				++(*this);
				return result;
			}

			friend bool operator==(const singly_ll_iterator& left, const singly_ll_iterator& right)
			{
				return left.ptr == right.ptr;
			}

			friend bool operator!=(const singly_ll_iterator& left, const singly_ll_iterator& right)
			{
				return left.ptr != right.ptr;
			}
		};

		// 4. add iterator
		singly_ll_iterator begin() { return singly_ll_iterator(head); }
		singly_ll_iterator end() { return singly_ll_iterator(NULL); }
		singly_ll_iterator begin() const { return singly_ll_iterator(head); }
		singly_ll_iterator end() const { return singly_ll_iterator(NULL); }

		// 5. add constructor
		singly_ll() = default;
		singly_ll(const singly_ll& other) : head(NULL)	
		{
			if (other.head)
			{
				head = new node{0, NULL};
				auto cur = head;					// cur --> cur->next
				auto it = other.begin();			// it(tmp) --> ++tmp
				while (true)
				{
					cur->data = *it;

					auto tmp = it;
					++tmp;
					if (tmp == other.end())
						break;

					cur->next = new node{0, NULL};
					cur = cur->next;
					it = tmp;
				}
			}	
		}

		singly_ll(const std::initializer_list<int>& ilist) : head(NULL)
		{
			for (auto it = std::rbegin(ilist); it != std::rend(ilist); it++)
				push_front(*it);
		}
};



int main()
{
	singly_ll sll = { 1, 2, 3 };
	sll.push_front(0);

	std::cout << "1st list : ";
	for (auto i : sll)
		std::cout << i << " ";
	std::cout << std::endl;

	auto sll2 = sll;
	sll2.push_front(-1);
	std::cout << "copy 1st list (after push_front -1) : ";
	for (auto i : sll2)
		std::cout << i << ' ';
	std::cout << std::endl;

	std::cout << "1st list after deep copy : ";

	for (auto i : sll)
		std::cout << i << ' ';
	std::cout << std::endl;

}
